import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from sklearn import svm

# Import the data
train_data = pd.read_csv('train.csv', sep=',',header=0)