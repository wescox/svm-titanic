import numpy as np
import pandas as pd
from math import exp, log
from random import randint

import matplotlib.pyplot as plt
from sklearn import svm

# Import the data
train_data = pd.read_csv('usedCar_trainingData.csv', sep=',',header=0)
train_data['lemon'] = np.where(train_data['lemon']==0,-1,1)

features = ['mileage','num_services']

def normalize_features(input_data):

	# Only scale the columns our algorithm will be learning from
	cols = ['mileage','num_services']

	# Calculate the L2 norm for each column
	l2norm = np.sqrt(np.sum(np.square(input_data[cols]), axis=0))
	# Normalize the data
	normalized_train_data = input_data.copy()
	normalized_train_data[cols] = normalized_train_data[cols].divide(l2norm)

	return normalized_train_data, l2norm

def plot_data():
	# Identify which datapoints are lemons
	good_cars = train_data[train_data['lemon'] == -1]
	bad_cars = train_data[train_data['lemon'] == 1]
	X = train_data['mileage']
	Y = train_data['num_services']
	normX = normalized_data['mileage']
	normY = normalized_data['num_services']
	contour_steps = 1/500

	# Scale the data back to its real values before plotting
	plt.plot(good_cars['mileage']/1000, good_cars['num_services'], 'ro')
	plt.plot(bad_cars['mileage']/1000, bad_cars['num_services'], 'bo')

	plt.xlabel('Mileage (1,000 miles)')
	plt.ylabel('Number of Services')

	# Plot the support vectors
	plt.plot(train_data['mileage'][lin_clf.support_]/1000,train_data['num_services'][lin_clf.support_],'ko', \
		markerfacecolor='none', markeredgecolor=(0,0,0,1.0), markersize=12)

	# Calculate line of best fit

	x_min, x_max = X.min() - 0.01*(X.max() - X.min()), X.max() + 0.01*(X.max() - X.min())
	y_min, y_max = Y.min() - 0.01*(Y.max() - Y.min()), Y.max() + 0.01*(Y.max() - Y.min())
	norm_x_min, norm_x_max = normX.min() - 0.01*(normX.max() - normX.min()), normX.max() + 0.01*(normX.max() - normX.min())
	norm_y_min, norm_y_max = normY.min() - 0.01*(normY.max() - normY.min()), normY.max() + 0.01*(normY.max() - normY.min())
	x1, x2 = np.meshgrid(np.arange(x_min, x_max, x_max*contour_steps), np.arange(y_min, y_max, y_max*contour_steps))
	norm_x1, norm_x2 = np.meshgrid(np.arange(norm_x_min, norm_x_max, norm_x_max*contour_steps), np.arange(norm_y_min, norm_y_max, norm_y_max*contour_steps))

	# print("x1 " + str(x1.shape))
	# print("x1 ravel " + str(np.array([1,2,4]).shape))


	xPredict = pd.DataFrame({'mileage': norm_x1.ravel(), 'num_services': norm_x2.ravel()})
	# print("xPredict " + str(xPredict))

	print("Decision function " + str(lin_clf.decision_function(xPredict)))

	Z2 = lin_clf.decision_function(xPredict).reshape(x1.shape)


	# Straighten out the array for each feature
	Z = lin_clf.predict(xPredict)
	Z = Z.reshape(x1.shape)
	print("x1 " + str(x1.max()))
	print("Z " + str(Z.shape))
	# print("Z2 gradient " + str(Z2gradient.shape))

	CS = plt.contourf(x1/1000, x2, Z2, cmap='RdBu_r', alpha=0.4, linestyles=['--','-','--'], levels=[-1,0,1])
	plt.colorbar()



	# w = lin_clf.coef_[0]
	# x1 = np.linspace(0,max(train_data['mileage'])/l2norm['mileage'])
	# x2 = (-w[0]*x1 - lin_clf.intercept_[0])/w[1]

	# l2norm_w = np.sqrt(np.sum(np.square(w)))
	# margin = 2 / l2norm_w
	# print("Margin " + str(margin))
	# print("Norm mileage min " + str(min(normalized_data['num_services'])) + " max " + str(max(normalized_data['mileage'])))
	# print("Norm num_services min " + str(min(normalized_data['mileage'])) + " max " + str(max(normalized_data['num_services'])))

	# plt.plot(x1*l2norm['mileage'], x2*l2norm['num_services'], 'k-')

	plt.legend(['Good', 'Bad', 'Support Vectors'])

	plt.show()



# Scale the data between 0 and 1, and remember the previous min and max values
normalized_data,l2norm = normalize_features(train_data)

# print(normalized_data[features])

lin_clf = svm.SVC(kernel = 'linear', C=1000)
print(lin_clf.fit(normalized_data[features], normalized_data['lemon']))

plot_data()
